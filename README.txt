INSTALACIÓN
===========

Falta un recurso en el proyecto
La clave API de TMBD ha sido añadida a un archivo de recursos xml que se ha incluido en el .gitignore para no compartirla en un repositorio publico

Para poder ejecutar el proyecto basta añadir un archivo /app/src/main/res/popularFilms/api_key.xml con el siguiente contenido:

<?xml version="1.0" encoding="utf-8"?>
<resources>
    <string name="tmbd_api_key">tu_clave_de_tmbd_aquí</string>
</resources>
