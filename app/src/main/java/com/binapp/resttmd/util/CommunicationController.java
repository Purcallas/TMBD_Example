package com.binapp.resttmd.util;

import android.content.Context;
import android.util.Log;

import com.binapp.resttmd.R;
import com.binapp.resttmd.gui.activity.MainActivity;
import com.binapp.resttmd.model.TMDBResponse;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static android.content.ContentValues.TAG;

/**
 * Created by victor on 27/2/17.
 *
 * This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

public class CommunicationController{

    //private static final String BASE_URL_API_4 = "https://api.themoviedb.org/4/";
    private static final String BASE_URL_API_3 = "https://api.themoviedb.org/3/";
    public static final String BASE_URL_IMAGE = "https://image.tmdb.org/t/p/w500";

    private static String API_KEY;
    private static final String SORT_CRITERIA = "popularity.desc";
    private static int pageNumber = 1;
    private static int totalPages = 2;
    private static int pageNumberSearch = 1;
    private static int totalPagesSearch = 2;
    private String searchWordQuery;
    private Context context;


    /*********************
     * PUBLIC METHODS
     *********************/

    /**
     * Constructor
     * @param context the activity to get the API_KEY from resources
     */
    public CommunicationController(Context context){

        this.context = context;
        //Add key

        //I do it in this way to maintain the private key outside the git public project, into the .gitignore
        //To add the key simply create a resource in res/values/strings or in new xml in res/values
        //with this format: <string name="tmbd_api_key">your_key_here</string>

        API_KEY = this.context.getResources().getString(R.string.tmbd_api_key);
    }

    /**
     * Reset static values
     */
    public void resetPopularFilms(){
        pageNumber = 1;
        totalPages = pageNumber;

    }

    /**
     * Reset static values
     */
    public void resetSearchFilms(){
        pageNumberSearch = 1;
        totalPagesSearch = pageNumberSearch;
    }

    /**
     * Get the popular films from the TMDB api
     */
    public void getAsyncFilms() {
        if (pageNumber<=totalPages){
            Gson gson = new GsonBuilder()
                    .setLenient()
                    .create();

            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL_API_3)
                    .addConverterFactory(GsonConverterFactory.create(gson))
                    .build();

            TheMovieDataBaseAPI tmdbAPI = retrofit.create(TheMovieDataBaseAPI.class);
            Call call = tmdbAPI.getList(API_KEY,pageNumber,SORT_CRITERIA);
            call.enqueue(new Callback() {
                @Override
                public void onResponse(Call call, Response response) {
                    if(response.isSuccessful()) {
                        TMDBResponse tmdbResponse = (TMDBResponse) response.body();
                        totalPages = tmdbResponse.getTotalPages();
                        MainActivity mainActivity = (MainActivity) context;
                        pageNumber++;   //Add a page number
                        mainActivity.filmsReceiver(tmdbResponse.getFilms(), totalPages<=pageNumber);

                    } else {
                        Log.e(TAG,response.errorBody().toString());
                    }
                }

                @Override
                public void onFailure(Call call, Throwable t) {
                    Log.e(TAG,t.getStackTrace().toString());
                }
            });

        }
    }

    /**
     * Lauch search word to the TMBD api
     */
    public void getAsyncSearchFilms() {
        if (pageNumberSearch<=totalPagesSearch && searchWordQuery != null){
            Gson gson = new GsonBuilder()
                    .setLenient()
                    .create();

            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL_API_3)
                    .addConverterFactory(GsonConverterFactory.create(gson))
                    .build();

            TheMovieDataBaseAPI tmdbAPI = retrofit.create(TheMovieDataBaseAPI.class);
            Call call = tmdbAPI.searchByTitle(API_KEY,searchWordQuery,pageNumberSearch,SORT_CRITERIA);
            call.enqueue(new Callback() {
                @Override
                public void onResponse(Call call, Response response) {
                    if(response.isSuccessful()) {
                        TMDBResponse tmdbResponse = (TMDBResponse) response.body();
                        totalPagesSearch = tmdbResponse.getTotalPages();
                        MainActivity mainActivity = (MainActivity) context;
                        pageNumberSearch++;   //Add a page number
                        mainActivity.seachedFilmsReceiver(tmdbResponse.getFilms(), totalPagesSearch<=pageNumberSearch);


                    } else {
                        Log.e(TAG,response.errorBody().toString());
                    }
                }

                @Override
                public void onFailure(Call call, Throwable t) {
                    Log.e(TAG,t.getStackTrace().toString());
                }
            });

        }
    }

    /**
     * Reset the word and store it in class variable
     * @param searchWordQuery
     */
    public void setSearchWordQuery(String searchWordQuery) {
        if (searchWordQuery == "") this.searchWordQuery = null;
        else this.searchWordQuery = searchWordQuery;

    }
}
