package com.binapp.resttmd.util;

import com.binapp.resttmd.model.TMDBResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by victor on 27/2/17.
 *
 * Basic class to communicate with TMBD api services using retrofit2
 *
 * This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

public interface TheMovieDataBaseAPI {

    @GET("discover/movie")
    Call<TMDBResponse> getList(@Query("api_key") String apiKey, @Query("page") int page ,  @Query("sort_by") String sortBy );


    @GET("search/movie")
    Call<TMDBResponse> searchByTitle(@Query("api_key") String api_key , @Query("query") String searchQuery , @Query("page") int page ,  @Query("sort_by") String sortBy );

}
