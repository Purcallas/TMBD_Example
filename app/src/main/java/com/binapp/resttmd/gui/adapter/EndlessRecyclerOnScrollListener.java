package com.binapp.resttmd.gui.adapter;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;

/**
 * EndlessRecyclerOnScrollListener
 *
 * This class is an abstract interface that allows to make and endless recyclerView adapter with scroll interaction
 *
 * Created by victor on 28/2/17.
 *
 * This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

public abstract class EndlessRecyclerOnScrollListener extends RecyclerView.OnScrollListener {
    public static String TAG = EndlessRecyclerOnScrollListener.class.getSimpleName();

    private int previousTotal = 0; // The total number of items in the dataset after the last load
    private boolean loading = true; // True if we are still waiting for the last set of data to load.
    private int visibleThreshold = 0; // The minimum amount of items to have below your current scroll position before loading more.
    int firstVisibleItem, visibleItemCount, totalItemCount;

    private int current_page = 1;
    public int actualLast;

    private RecyclerView.LayoutManager mLinearLayoutManager;

    public EndlessRecyclerOnScrollListener(LinearLayoutManager linearLayoutManager) {
        this.mLinearLayoutManager = linearLayoutManager;
    }

    public EndlessRecyclerOnScrollListener(StaggeredGridLayoutManager staggeredGridLayoutManager) {
        this.mLinearLayoutManager = staggeredGridLayoutManager;
    }

    @Override
    public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
        super.onScrolled(recyclerView, dx, dy);
        visibleItemCount = recyclerView.getChildCount();
        totalItemCount = mLinearLayoutManager.getItemCount();
        if (mLinearLayoutManager.getClass().equals(LinearLayoutManager.class)){
            LinearLayoutManager llm = (LinearLayoutManager) mLinearLayoutManager;
            firstVisibleItem = llm.findFirstVisibleItemPosition();
            actualLast = llm.findLastCompletelyVisibleItemPosition()
            ;
        }else if (mLinearLayoutManager.getClass().equals(StaggeredGridLayoutManager.class)){
            StaggeredGridLayoutManager sgl = (StaggeredGridLayoutManager) mLinearLayoutManager;
            firstVisibleItem = sgl.findFirstVisibleItemPositions(null)[0];
        }


        if (loading) {
            if (totalItemCount > previousTotal) {
                loading = false;
                previousTotal = totalItemCount;
            }
        }
        if (!loading && (totalItemCount - visibleItemCount)
                <= (firstVisibleItem + visibleThreshold)) {
            // End has been reached
            // Do something
            current_page++;

            onLoadMore(current_page);

            loading = true;
        }

        onScroll(actualLast, firstVisibleItem);
    }

    public abstract void onLoadMore(int current_page);

    public abstract void onScroll(int actualLast,int firstVisibleItem);
}

