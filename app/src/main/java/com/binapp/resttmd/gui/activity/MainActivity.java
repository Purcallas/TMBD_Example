package com.binapp.resttmd.gui.activity;

import android.content.Context;
import android.graphics.Rect;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.inputmethod.InputMethodManager;

import com.binapp.resttmd.R;
import com.binapp.resttmd.gui.SearchFragment;
import com.binapp.resttmd.gui.adapter.EndlessRecyclerOnScrollListener;
import com.binapp.resttmd.gui.adapter.RecyclerFilmAdapterViewAdapter;
import com.binapp.resttmd.model.Film;
import com.binapp.resttmd.util.CommunicationController;

import java.util.ArrayList;

/**
 * This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
public class MainActivity extends AppCompatActivity {


    private ArrayList<Film> films;                                      //The popular films Array List
    private CommunicationController communicationController;             //The communication controller

    private RecyclerView recyclerViewPopularFilms;
    private RecyclerFilmAdapterViewAdapter recyclerViewAdapter;

    private SearchFragment searchFragment;                               //The search Fragment

    private Handler handler;                                            //To prevent timer on search
    private String searchWord;                                          //Search word to check in TMDB

    private FloatingActionButton floatingActionButton;                   //FLoating button to close keyboard when is showed


    /***************************
     * OVERRIDE METHODS
     ***************************/

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Init ArrayList
        this.films = new ArrayList<>();

        //Comunication Controller
        this.communicationController = new CommunicationController(this);

        //Link the recyclerView
        this.recyclerViewPopularFilms = (RecyclerView) this.findViewById(R.id.recycler_popular_films);
        //More columns test
        //It generates a conflict with the om.chauthai.overscroll:overscroll-bouncy:0.1.0 library
        //StaggeredGridLayoutManager llm = new StaggeredGridLayoutManager(1, 1);
        //this.recyclerViewPopularFilms.setLayoutManager(llm);

        LinearLayoutManager llm = new LinearLayoutManager(this);
        this.recyclerViewPopularFilms.setLayoutManager(llm);
        this.recyclerViewPopularFilms.scrollToPosition(0);
        this.recyclerViewPopularFilms.addItemDecoration(new DividerItemDecoration(this, 0));

        //Set the recyclerViewAdapter
        this.recyclerViewAdapter = new RecyclerFilmAdapterViewAdapter(this, this.films, false);
        this.recyclerViewPopularFilms.setAdapter(recyclerViewAdapter);

        //communicationController.getAsyncFilms();

        this.recyclerViewPopularFilms.setOnScrollListener(new EndlessRecyclerOnScrollListener(llm) {
            @Override
            public void onLoadMore(int current_page) {
                communicationController.getAsyncFilms();
            }

            @Override
            public void onScroll(int actualLast, int firstVisibleItem) {
                //Its only necessary for pagination
            }
        });


        //Init search Fragment
        this.searchFragment = new SearchFragment();

        //Init Handler
        this.handler = new Handler();

        //setFloatingButtonAndKeyboardBehaviour();

    }




    @Override
    protected void onStart() {
        super.onStart();
        if (this.films.size() == 0)this.communicationController.resetPopularFilms();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onResume() {
        super.onResume();

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu, menu);
        SearchView searchView = (SearchView) MenuItemCompat.getActionView(menu.findItem(R.id.action_search));
        //TextView queryTV = (TextView) searchView.findViewById(android.support.v7.appcompat.R.id.search_src_text);
        MenuItem searchItem = menu.findItem(R.id.action_search);


        MenuItemCompat.setOnActionExpandListener(searchItem, new MenuItemCompat.OnActionExpandListener() {
            @Override
            public boolean onMenuItemActionCollapse(MenuItem item) {
                removeSearchFragment();
                return true;
            }

            @Override
            public boolean onMenuItemActionExpand(MenuItem item) {
                //Launch the search Fragment
                launchSearchFragment();
                return true;
            }
        });

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {

            @Override
            public boolean onQueryTextChange(String newText) {
                searchWord = newText;
                if (searchFragment.getCommunicationController() != null){
                    handler.removeCallbacksAndMessages(null);
                    //Add handler to prevent crash
                    //There provoques a crash by multiple adapter thread call
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            if (searchWord.equals("")) searchFragment.deleteSearchedFilms(false);
                            else searchFragment.deleteSearchedFilms(true);

                            searchFragment.getCommunicationController().resetSearchFilms();
                            searchFragment.getCommunicationController().setSearchWordQuery(searchWord);
                            searchFragment.getCommunicationController().getAsyncSearchFilms();
                        }
                    }, 1000);
                    return true;




                }
                return true;
            }

            @Override
            public boolean onQueryTextSubmit(String query) {
                //Close the keyboard
                View contentView = findViewById(android.R.id.content);
                InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(contentView.getWindowToken(), 0);
                return true;
            }

        });

        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_search) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    /***************************
     * PUBLIC METHODS
     ***************************/

    /**
     * Receive the popular films from CommunicationController and add they to the list
     * associated with the ReciclerView
     *
     * @param films ArrayList<Film> films recevied from the Comunication controller
     * @param isFinish true if all the films are arlready recevied from the Communicetion Controller
     */
    public void filmsReceiver(ArrayList<Film> films, boolean isFinish) {
        this.films.addAll(films);
        this.recyclerViewAdapter.hideFooter(isFinish);  //The footer is the loading image. If is finish the loadding image will be hide.
        //Notify the reciclerView that new films are added
        recyclerViewAdapter.notifyItemInserted(this.films.size() - 1);  //Notify the changes

    }

    public void seachedFilmsReceiver(ArrayList<Film> films, boolean isFinish) {
        this.searchFragment.addSearchedFilms(films, isFinish);
    }

    /***************************
     * PRIVATE METHODS
     ***************************/

    /**
     * Launch the search fragment without pre defined word
     */
    private void launchSearchFragment() {
        getSupportFragmentManager().beginTransaction().setCustomAnimations(R.anim.slide_in_down, 0).replace(R.id.search_frame_content, this.searchFragment).commit();
    }

    /**
     * Launch the search fragment without pre defined word
     */
    private void removeSearchFragment() {
        getSupportFragmentManager().beginTransaction().setCustomAnimations(0, R.anim.slide_out_up).remove(this.searchFragment).commit();
    }

    /**
     * When keyboard is shown a floatting button to close it is always shown
     */
    /*
    There are a bug in Android relating the fragments animation and the adjustResize window style that show flickering in animations

    private void setFloatingButtonAndKeyboardBehaviour() {
        final View contentView = this.findViewById(android.R.id.content);
        floatingActionButton = (FloatingActionButton) this.findViewById(R.id.fab);
        floatingActionButton.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                //Close keyboard
                InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(contentView.getWindowToken(), 0);
                return false;
            }
        });
        //Show and hide floating button on keyboard appears
        contentView.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {

                Rect r = new Rect();
                contentView.getWindowVisibleDisplayFrame(r);
                int screenHeight = contentView.getRootView().getHeight();

                // r.bottom is the position above soft keypad or device button.
                // if keypad is shown, the r.bottom is smaller than that before.
                int keypadHeight = screenHeight - r.bottom;
                if (keypadHeight > screenHeight * 0.10) { // 0.15 ratio is perhaps enough to determine keypad height.
                    floatingActionButton.setVisibility(View.VISIBLE);
                }
                else {
                    floatingActionButton.setVisibility(View.INVISIBLE);
                }

            }
        });
        floatingActionButton.setVisibility(View.INVISIBLE);
    }
    */



}


