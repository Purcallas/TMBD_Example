package com.binapp.resttmd.gui;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.binapp.resttmd.R;
import com.binapp.resttmd.gui.activity.MainActivity;
import com.binapp.resttmd.gui.adapter.EndlessRecyclerOnScrollListener;
import com.binapp.resttmd.gui.adapter.RecyclerFilmAdapterViewAdapter;
import com.binapp.resttmd.model.Film;
import com.binapp.resttmd.util.CommunicationController;

import java.util.ArrayList;


/**
 * Created by victor on 28/2/17.
 *
 * Fragment that shows the search list view with counter.
 *
 * This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

public class SearchFragment extends Fragment {

    private RecyclerView recyclerSearchedFilms;
    private RecyclerFilmAdapterViewAdapter recyclerViewAdapter;
    private CommunicationController communicationController;
    private ArrayList<Film> searchedFilms;
    private LinearLayoutManager linearLayoutManager;
    private TextView pageNumber;
    private int actualPage = 1;                                                     //Actual Page to be showed in UI
    private EndlessRecyclerOnScrollListener endlessRecyclerOnScrollListener;

    /***********************+
     * OVERRIDE METHODS
     ***********************/

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             final Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.frame_search_layout,
                container, false);

        MainActivity mainActivity = (MainActivity) getActivity();
        this.communicationController = new CommunicationController(mainActivity);
        this.communicationController.resetSearchFilms();
        this.searchedFilms = new ArrayList<>();

        //Link the elements
        this.recyclerSearchedFilms = (RecyclerView) view.findViewById(R.id.recycler_search_films);
        this.pageNumber = (TextView) view.findViewById(R.id.page_number);
        actualPage = 1;
        this.pageNumber.setText(""+(actualPage-1));

        Button prevCarousel = (Button) view.findViewById(R.id.prevButton);
        prevCarousel.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_UP){
                    if (actualPage>0){
                        actualPage--;
                        recyclerSearchedFilms.scrollToPosition(actualPage*20-20);
                    }
                }
                return false;
            }
        });

        Button nextCarousel = (Button) view.findViewById(R.id.nextButton);
        nextCarousel.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_UP){
                    recyclerSearchedFilms.scrollToPosition(actualPage*20);
                    if ((actualPage+1)*20 > searchedFilms.size())communicationController.getAsyncSearchFilms();
                    if ((actualPage+1)*20<searchedFilms.size()) {
                        actualPage++;
                    }



                }
                return false;
            }
        });


        this.linearLayoutManager = new LinearLayoutManager(mainActivity);
        this.recyclerSearchedFilms.setLayoutManager(linearLayoutManager);
        this.recyclerSearchedFilms.addItemDecoration(new DividerItemDecoration(getActivity(), 0));

        fillTheAdapter(mainActivity);

        return view;
    }


    /***********************+
     * PUBLIC METHODS
     ***********************/

    public void deleteSearchedFilms(boolean isLoading) {
        if (this.searchedFilms != null) {
            this.searchedFilms.clear();
            fillTheAdapter((MainActivity) getActivity());
            this.recyclerViewAdapter.hideFooter(!isLoading);

        }
    }

    public CommunicationController getCommunicationController() {
        return communicationController;
    }

    /**
     * Fill the recyclerView List with films
     * Get isFinnish parameter to determine if the Loading image (footer) must be showed
     * @param films partial ArrayList of films
     * @param isFinish  true if all the films are received from the CommunicationController (TMBD API)
     */
    public void addSearchedFilms(ArrayList<Film> films, boolean isFinish) {
        if (films != null && films.size() > 0) {
            this.searchedFilms.addAll(films);
            //Hide or Show depending if  finish is reached
            this.recyclerViewAdapter.hideFooter(isFinish);
            this.recyclerViewAdapter.notifyItemInserted(this.searchedFilms.size() - 1);
        }else if (films.size() == 0){
            //Refresh and Hide loader when response is empty
            deleteSearchedFilms(false);
        }

    }

    /***********************+
     * PRIVATE METHODS
     ***********************/

    /**
     * Set the adapter
     * Will be called if a adapter refresh must be needed
     * @param mainActivity
     */
    private void fillTheAdapter(MainActivity mainActivity) {
        this.recyclerSearchedFilms.clearOnScrollListeners();
        //Endless scroll
        this.endlessRecyclerOnScrollListener = new EndlessRecyclerOnScrollListener(linearLayoutManager) {
            @Override
            public void onLoadMore(int current_page) {
                if (current_page>2)communicationController.getAsyncSearchFilms(); //Prevent to launch in int , initial was lauched from keyboard
            }

            @Override
            public void onScroll(int actualLast, int firstVisibleItem) {
                if (actualLast != -1){
                    actualPage = actualLast/20+1;
                    pageNumber.setText(""+actualLast);
                }
            }
        };

        this.recyclerSearchedFilms.setOnScrollListener(this.endlessRecyclerOnScrollListener);

        //Build the recyclerViewAdapter
        this.recyclerViewAdapter = new RecyclerFilmAdapterViewAdapter(mainActivity, searchedFilms, true);
        //Set the adapter
        if (recyclerSearchedFilms.getAdapter() == null)
            this.recyclerSearchedFilms.setAdapter(recyclerViewAdapter);
        else this.recyclerSearchedFilms.swapAdapter(recyclerViewAdapter, true);
        //Hide the on loader
        this.recyclerViewAdapter.hideFooter(true);

        actualPage = 1;

    }




}

