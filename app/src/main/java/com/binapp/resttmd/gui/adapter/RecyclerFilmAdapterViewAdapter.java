package com.binapp.resttmd.gui.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.binapp.resttmd.R;
import com.binapp.resttmd.model.Film;
import com.binapp.resttmd.util.CommunicationController;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * RecyclerFilmAdapterViewAdapter
 *
 * Basic RecyclerViewAdapter with ViewHolder pattern that uses a Footer with loading image.
 * Also add a specific layout depending if receives the isSearch variable on true or false
 * Created by victor on 22/2/17.
 *
 * This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

public class RecyclerFilmAdapterViewAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    //Item type (Could be a header,an item or a footer)
    private static final int TYPE_ITEM = 0;
    private static final int TYPE_FOOTER = 1;

    private final ArrayList<Film> popularFilms;
    private final Context context;

    private boolean showFooter = true;
    private boolean isSearch;


    public ArrayList<Film> getPopularFilms() {
        return popularFilms;
    }

    public RecyclerFilmAdapterViewAdapter(Context context, ArrayList<Film> popularFilms, boolean isSearch) {
        this.context = context;
        this.popularFilms = popularFilms;
        this.isSearch = isSearch;
    }

    public void hideFooter(boolean hideFooter){
        this.showFooter = !hideFooter;
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if(viewType == TYPE_FOOTER) {
            View v = LayoutInflater.from (parent.getContext ()).inflate (R.layout.row_film_load_layout, parent, false);
            return new FooterViewHolder (v);
        } else if(viewType == TYPE_ITEM) {
            int layout = R.layout.row_popular_film_layout;
            if (isSearch) layout =  R.layout.row_search_film_layout;
            View v = LayoutInflater.from (parent.getContext ()).inflate (layout, parent, false);
            return new ItemViewHolder (v);
        }
        return null;
    }

    @Override
    public int getItemViewType (int position) {
        if(isPositionFooter (position)) {
            return TYPE_FOOTER;
        }
        return TYPE_ITEM;
    }

    private boolean isPositionFooter (int position) {
        return position == popularFilms.size () + 1;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {


        if (position<popularFilms.size()){
            ItemViewHolder itemHolder = (ItemViewHolder) holder;
            Film film = popularFilms.get(position);
            itemHolder.title.setText(film.getOriginal_title());
            itemHolder.date.setText(film.getRelease_date());
            itemHolder.overview.setText(film.getOverview());
            //Must have the BASE_URL_IMAGE static from CommunicationController
            Picasso.with(context).load(CommunicationController.BASE_URL_IMAGE + film.getPoster_path()).into(itemHolder.filmImage);

        }

        /*
        // Capture the touches
        holder.foregroundView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                return true;
            }
        });
        */
    }


    @Override
    public int getItemCount() {

        if (showFooter) return popularFilms.size() + 1;
        return popularFilms.size();
    }

    class ItemViewHolder extends RecyclerView.ViewHolder {
        protected ImageView filmImage;
        protected TextView title;
        protected TextView date;
        protected TextView overview;
        protected RelativeLayout foregroundView;

        public ItemViewHolder(View itemView) {
            super(itemView);
            filmImage = (ImageView) itemView.findViewById(R.id.iv_film_image);
            title = (TextView) itemView.findViewById(R.id.tv_title);
            date = (TextView) itemView.findViewById(R.id.tv_release_date);
            overview = (TextView) itemView.findViewById(R.id.tv_overwiev);
            foregroundView = (RelativeLayout) itemView.findViewById(R.id.foreground);
        }

    }

     class FooterViewHolder extends RecyclerView.ViewHolder {
        public FooterViewHolder(View itemView) {
            super(itemView);
        }

    }


}
