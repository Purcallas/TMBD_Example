package com.binapp.resttmd.model;

import com.google.gson.GsonBuilder;

/**
 * Created by victor on 27/2/17.
 *
 * Film
 *
 * Has the basic data from the film
 *
 * This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

public class Film {
    private String poster_path;
    private String release_date;
    private String original_title;
    private String overview;

    /*********************
     * CONSTRUCTOR
     *********************/


    public Film(String poster_path, String release_date, String original_title, String overview) {
        this.poster_path = poster_path;
        this.release_date = release_date;
        this.original_title = original_title;
        this.overview = overview;
    }

    /*********************
     * GETTERS AND SETTERS
     *********************/

    public String getPoster_path() {
        return poster_path;
    }

    public void setPoster_path(String poster_path) {
        this.poster_path = poster_path;
    }

    public String getRelease_date() {
        return release_date;
    }

    public void setRelease_date(String release_date) {
        this.release_date = release_date;
    }

    public String getOriginal_title() {
        return original_title;
    }

    public void setOriginal_title(String original_title) {
        this.original_title = original_title;
    }

    public String getOverview() {
        return overview;
    }

    public void setOverview(String overview) {
        this.overview = overview;
    }

    @Override
    public String toString() {
        return new GsonBuilder().create().toJson(this, Film.class);
    }
}
